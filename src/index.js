import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';

import MainLayout from './MainLayout';

import './css/index.css';

ReactDOM.render((
    <BrowserRouter>
        <Route path="/" component={MainLayout}>
        </Route>
    </BrowserRouter>
), document.getElementById('root'));