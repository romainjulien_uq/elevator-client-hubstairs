/**
 * Cette classe initialize et contient la socket connectée au serveur.
 * (initialisée de index.html)
 */

const io = require('socket.io-client');
const socket = io.connect('http://localhost:3001');

socket.on('connection', function(data){
    console.log(data);
});

socket.on('error', function (data) {
    console.log('error: ' + data);
});

socket.on('connect_error', function (data) {
    console.log('Cannot connect to server: ' + data);
});

socket.on('reconnect_attempt', function (data) {
    if (data === 5) {
        socket.disconnect(); // Empeche la reconnection automatique après 5 essais
        // TODO Prevenir utilisateur et ouvrir popup pour retenter la reconnection manuellement
    }
});

socket.on('disconnect', function(data) {
    console.log('Connection is lost: ' + data);
});

exports.socket = socket;

