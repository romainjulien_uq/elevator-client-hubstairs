import React, { Component } from 'react';

var Connection = require('../Connection');

import '../css/admin.css';

/**
 * Le component parent de la page Admin.
 * Cette partie de l'application peut etre grandement optimisée:
 * - ajouter des filtres (date, en particulier)
 * - n'afficher que les 10 premiers elements dans le tableau (avec des fleches 'suivant'/'retour'
 * - ameliorer le format du timestamp pour etre mieux lisible
 */
class Admin extends Component {

    constructor(props) {
        super(props);

        this.state = {
            logRows: []
        };

        let timestamp = new Date();
        let collection = timestamp.getDay() + "-" + timestamp.getMonth() + "-" + timestamp.getFullYear();
        Connection.socket.emit('request activity log', {collection: collection});

        Connection.socket.on('response activity log', (data) => this.processListLogs(data));
    }

    processListLogs(data) {

        var logRows = [];

        // Inverse la liste pour avoir les données les plus recentes en haut du tableau
        for (var i = (data.allLogs.length - 1); i >= 0; i--) {
            var item = data.allLogs[i];
            logRows.push(<LogRow floor={item.floor} activity={item.activity}
                                 timestamp={item.timestamp.toString().replace(/T/, ' ').replace(/\..+/, '')} />);
        }

        this.setState({logRows:logRows});
    }

    render() {
        return (
            <table className="log-table">
                {this.state.logRows}
            </table>
        );
    }
}

/**
 * Une ligne du tableau.
 */
class LogRow extends Component {

    render() {

        return (
            <tr>
                <td className="floor-cell"><p className="caption">{'Etage ' + this.props.floor}</p></td>
                <td className="activity-cell"><p>{this.props.activity}</p></td>
                <td className="timestamp-cell"><p>{this.props.timestamp}</p></td>
            </tr>
        );

    }
}
export default Admin;