import React, { Component } from 'react';

import '../css/panneau-interne.css';
var Connection = require('../Connection');

/**
 * Le component parent du panneau interne. Divisé en 2, sur la gauche un tableau avec les 10 boutons,
 * sur la droite les voyants indiquants la direction de l'ascenseur et l'ouverture des portes.
 */
class PanneauInterne extends Component {

    render() {

        var rows = [];
        for (var i=4; i >= 0; i--) {
            rows.push(<PanneauRow rowNumber={i}/>);
        }

        return (
            <div className="panneau-main">
                <table className="bouton-table">
                    {rows}
                </table>
                <div className="indicator-container">
                    <div className="direction-indicator-container">
                        <div className="arrow-wrapper">
                            <ArrowElevatorDirection direction="up"/>
                            <ArrowElevatorDirection direction="down"/>
                        </div>
                    </div>
                    <div className="door-indicator-container">
                        <ElevatorDoors/>
                    </div>
                </div>
                <div className="clear-float"></div>
            </div>
        );
    }
}

/**
 * Une ligne du tableau contenants les boutons
 */
class PanneauRow extends Component {

    render() {
        
        return (
            <tr>
                <td>
                    <BoutonPanneau floorNumber={this.props.rowNumber * 2}/>
                </td>
                <td>
                    <BoutonPanneau floorNumber={this.props.rowNumber * 2 + 1}/>
                </td>
            </tr>
        );
    }
}

/**
 * Le component representant les boutons du panneau
 * Lorsqu'un bouton est cliqué, une notification est envoyé au serveur.
 * Lorsque l'ascenseur s'arrete, chaque bouton recoit un event et verifie a quel etage l'ascenseur
 * s'est arrété, puis éteind le voyant si necessaire.
 */
class BoutonPanneau extends Component {

    constructor(props) {
        super(props);
        this.state = {
            floorNumber: props.floorNumber,
            select: false
        };

        this.handleClick = this.handleClick.bind(this);

        Connection.socket.on('elevator stop', (data) => this.handleElevatorStop(data));
    }

    handleElevatorStop(data) {
        if (this.state.floorNumber === data.floor) {
            this.setState({select: false});
        }
    }

    handleClick() {
        if (this.state.select) {
            return;
        }

        Connection.socket.emit('elevator called', {floor: this.state.floorNumber});
        this.setState({select: true});
    }

    render() {

        return (
            <div className={'bouton-panneau' + (this.state.select ? ' bouton-panneau-select' : '')}
                 onClick={this.handleClick} >
                <p className="caption">{this.props.floorNumber}</p>
            </div>
        );
    }
}

/**
 * Le component representant les fleches indiquants la direction de l'ascenseur.
 * Ce component recoit une notification lorsque l'ascenseur demarre, et allume la
 * fleche correspondante. Lorsque l'ascenseur s'arrete, les voyants s'éteignent.
 */
class ArrowElevatorDirection extends Component {

    constructor(props) {
        super(props);
        this.state = {
            directionUp: props.direction === "up",
            select: false
        };

        Connection.socket.on('elevator start', (data) => this.handleElevatorStart(data));
        Connection.socket.on('elevator stop', () => this.handleElevatorStop());
    }

    handleElevatorStart(data) {
        if (data.isGoingUp === this.state.directionUp) {
            this.setState({select: true});
        }
    }

    handleElevatorStop() {
        this.setState({select: false});
    }

    render() {

        return (
            <div className={'arrow' + (this.state.select ? ' arrow-select' : '')}>
                <span className={this.state.directionUp ? "up" : "down"}></span>
            </div>
        );
    }

}

/**
 * Le component representant les portes de l'ascenseur.
 * L'animation se fait en utilisant CSS3, ce component est chargé d'ajouter la classe CSS
 * correspondant a l'animation recherchée.
 * Les portes s'ouvrent lorsque l'ascenseur s'arrete - et restent ouvertes si il n'y a pas
 * d'autres etages ou aller. Elles se ferment lorsque l'ascenseur demarre.
 */
class ElevatorDoors extends Component {

    constructor() {
        super();
        this.state = {
            open: true
        };

        Connection.socket.on('elevator stop', () => this.openDoors());
        Connection.socket.on('elevator start', () => this.closeDoors());
    }

    openDoors() {
        this.setState({open: true});
    }

    closeDoors() {
        this.setState({open: false});
    }

    render() {

        var doorLeftClass = 'door door-left ';
        var doorRightClass = 'door door-right ';

        if(this.state.open){
            doorLeftClass += 'open-door-left';
            doorRightClass += 'open-door-right';
        } else {
            doorLeftClass += 'close-door';
            doorRightClass += 'close-door';
        }

        return (
            <div className="door-wrapper">
                <div className={doorLeftClass}>
                </div>
                <div className={doorRightClass}>
                </div>
            </div>
            
        );
    }
}

export default PanneauInterne;