import React, { Component } from 'react';
import '../css/elevator.css';

import PanneauInterne from './PanneauInterne.js';
import Immeuble from './Immeuble.js';

/**
 * Le component parent de la partie ascensseur.
 */
class Elevator extends Component {

    render() {

        return (
            <div className="app-container">
                <PanneauInterne/>
                <Immeuble/>
            </div>
        );
    }
}

export default Elevator;