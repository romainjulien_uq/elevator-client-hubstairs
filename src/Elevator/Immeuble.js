import React, { Component } from 'react';
import '../css/immeuble.css';
var Connection = require('../Connection');

/**
 * Le component parent du tableau representant l'immeuble.
 */
class Immeuble extends Component {

    render() {

        var tableRows = [];
        for (var i=9; i >= 0; i--) {
            tableRows.push(<ImmeubleRow floorNumber={i}/>);
        }

        return (
            <table className="immeuble-table">
                {tableRows}
            </table>
        );
    }
}

/**
 * Une ligne du tableau representant l'immeuble, chaque ligne a 3 colonnes: l'etage, un voyant
 * indiquant si l'ascenseur est arrété/passé par cet etage, et un bouton d'appel.
 */
class ImmeubleRow extends Component {

    render() {

        return (
            <tr>
                <td className="description"><p>{this.getFloorDescription(this.props.floorNumber)}</p></td>
                <td className="indicator"><RowIndicator floorNumber={this.props.floorNumber}/></td>
                <td className="button"><RowButton floorNumber={this.props.floorNumber}/></td>
            </tr>

        );
    }

    getFloorDescription(floorNumber) {

        var floorDescription;

        switch (floorNumber) {
            case 0:
                floorDescription = "Rez-de-chaussé";
                break;
            case 1:
                floorDescription = "1er Etage";
                break;
            default:
                floorDescription = floorNumber + "ème Etage";
                break;
        }

        return floorDescription;
    }
}

/**
 * Le component representant l'indicateur lumineux.
 * Lorsque l'ascenseur est en mouvement, le serveur envois une mise a jour de la position de
 * l'ascenseur toutes les secondes. Lorsque la notification est recue, ce component verifie que
 * l'ascenseur est a cet etage, si oui, allume le voyant, ou sinon l'éteind.
 */
class RowIndicator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rowNumber: props.floorNumber,
            on: false
        };

        Connection.socket.on('elevator at floor', (data) => this.elevatorIsAtFloor(data));
    }

    elevatorIsAtFloor(data) {
        if (data.floor === this.state.rowNumber) {
            this.setState({on: true});
        } else {
            this.setState({on: false});
        }
    }

    render() {

        return (
            <div className={'row-indicator' + (this.state.on ? ' row-indicator-on' : '')}>
            </div>
        );
    }
}

/**
 * Le component representant le bouton d'appel.
 */
class RowButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rowNumber: props.floorNumber,
            select: false
        };

        this.handleClick = this.handleClick.bind(this);
    }

    /**
     * Les spécifications indiquent qu'il n'est pas necessaire d'éteindre le bouton sur le panneau immeuble...?
     * Cette methode n'est donc jamais appelée.
     * @param floor
     */
    handleElevatorStoppedAtFloor(floor) {
        if (this.state.rowNumber === floor) {
            this.setState({select: false});
        }
    }

    handleClick() {
        if (this.state.select) {
            return;
        }

        Connection.socket.emit('elevator called', {floor: this.state.rowNumber});
        this.setState({select: true});
    }

    render() {

        return (
            <div className={'row-button' + (this.state.select ? ' row-button-select' : '')}
                 onClick={this.handleClick}>
            </div>
        );
    }

}

export default Immeuble;