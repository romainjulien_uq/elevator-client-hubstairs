import React, { Component } from 'react';
import { Link, Switch, Route } from 'react-router-dom';
import './css/main.css';

import Elevator from './Elevator/Elevator';
import Admin from './Admin/Admin';

/**
 * Le component parent de la page.
 * Contient le menu, et affiche suivant la selection la partie ascensseur ou la partie admin.
 */
class MainLayout extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectLeft: true,
            selectRight: false
        };

        this.handleClickLeft = this.handleClickLeft.bind(this);
        this.handleClickRight = this.handleClickRight.bind(this);
    }

    handleClickLeft() {
        if (!this.state.selectLeft) {
            this.setState({selectLeft: true});
            this.setState({selectRight: false});
        }
    }

    handleClickRight() {
        if (!this.state.selectRight) {
            this.setState({selectRight: true});
            this.setState({selectLeft: false});
        }
    }


    render() {
        return (
            <div className="main-container">
                <div className="nav-bar">
                    <NavElement select={this.state.selectLeft} linkTo="/" position="left" label="Home"
                                onClick={this.handleClickLeft} />
                    <NavElement select={this.state.selectRight} linkTo="/admin" position="right" label="Admin"
                                onClick={this.handleClickRight}/>
                </div>
                <div className="content">
                    <Switch>
                        <Route exact path='/' component={Elevator}/>
                        <Route path='/admin' component={Admin}/>
                    </Switch>
                </div>
            </div>
        )
    }
}

/**
 * Un element de la barre de navigation.
 */
class NavElement extends Component {

    render() {
        var linkClass = 'nav-element nav-'
            + this.props.position
            + ' caption'
            + (this.props.select ? ' nav-select' : '');

        return(
            <Link className={linkClass} to={this.props.linkTo} onClick={this.props.onClick}>
                {this.props.label}
            </Link>
        );
    }
}
export default MainLayout;